import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import LoginPage from './pages/login';
import LandingPage from './pages/landing';
import SignUpPage from './pages/signup';
import './styles/base.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/login/:accountType" component={(props => <LoginPage {...props} />)}></Route>
        <Route path="/signup/:accountType" component={(props => <SignUpPage {...props} />)}></Route>
        <Route path="/" component={(props) => <LandingPage {...props}/>}></Route>
      </Switch>
    </Router>

  );
}

export default App;
