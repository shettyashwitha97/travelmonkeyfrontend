import styled from 'styled-components';
import library from "../assets/loginbg4.jpeg";

var PrimaryColor = "#3c3c3c";

export const Heading = styled.div`
    font-size: 24px;
    text-align: center;
    font-weight: bold;
    margin: 20px auto 40px;
`;

export const ThemeBackground = styled.div`
    width: 100vw;
    height: 100vh;
    background-image: url(${library});
    background-position: center;
    display: flex;
    align-items: center;
    position: relative;
    background-repeat: no-repeat;
    background-size: cover;
    &:before {
        position: absolute;
        width: 100%;
        height: 100%;
        content: '';
        background: rgb(125 124 125 / 50%);
    }
`;

export const LoginCard = styled.div`
    margin: auto;
    background: white;
    padding: 20px;
    width: 400px;
    max-width: 80%;
    z-index: 2;
    position: relative;
`;

export const BrandLogo = styled.img`
    width: 80px;
    height: 80px;
    display: block;
    margin: auto;
`;

export const Input = styled.input`
    display: block;
    background: #f3f3f3;
    border: 1px solid #a9a8a8;
    width: 100%;
    margin: auto;
    margin: 17px auto;
    border-radius: 3px;
    padding: 14px;
    font-size: 16px;
    box-sizing: border-box;
`;

export const Link = styled.a`
    text-decoration: none;
    color: #0D95DC;
    cursor: pointer;
`;

export const LeftAlignLink = styled(Link)`
    text-align: right;
    display: block;
`;

export const Button = styled.button`
    background: #0FAE63;
    border: 2px solid #0FAE7B;
    font-size: 16px;
    color: #FFFFFF;
    text-align: center;
    border-radius: 3px;
    padding: 10px;
    width: 100%;
    margin-top: 20px;
    font-weight: bold;
    cursor: pointer;
    margin-bottom: 10px;
`;

export const ButtonSecondary = styled.button`
    background: white;
    border: 2px solid #0FAE63;
    font-size: 16px;
    color: #0FAE63;
    text-align: center;
    border-radius: 3px;
    padding: 10px;
    width: 100%;
    font-weight: bold;
    cursor: pointer;
    margin-bottom: 10px;
`;


export const Divider = styled.div`
    height: 1px;
    width: 100%;
    background: #e9e9e9;
`;

export const DividerWithText = styled.div`
    position: relative;
    height: 30px;
    margin-top: 20px;
`;

export const Text = styled.div`
    background: white;
    font-size: 14px;
    text-align: center;
    position: absolute;
    left: 30%;
    top: -9px;
    padding: 0 10px;
`;

export const CloseButton = styled.img`
    width: 25px;
    height: 25px;
    position: absolute;
    right: 15px;
    top: 15px;
    opacity: 0.5;
    cursor: pointer;
`;