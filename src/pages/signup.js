import React from 'react';
import { Heading, CloseButton, LoginCard, ThemeBackground, Input, Button } from '../styles/LoginPageStyles';
import CloseIcon from "../assets/close-icon.png"; 

class SignUpPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            accountType: this.props.match.params.accountType
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.accountType !== prevProps.match.params.accountType) {
            this.setState({
                accountType: this.props.match.params.accountType
            })
        }
    }

    goToPage = () => {
        window.location.href = "/landing";
    }

    render() {
        const { accountType } = this.state;
        return <ThemeBackground>

            <LoginCard>
                <CloseButton src={CloseIcon} onClick={this.goToPage}></CloseButton>
                {/* <BrandLogo src={brandlogo}/> */}
                <Heading>Welcome to TravelMonkey!</Heading>
                <Input placeholder="Name" type="text" />
                <Input placeholder="Email" type="text" />
                <Input placeholder="Mobile number" type="number" />
                <Input placeholder="Password" type="password" />
                <Input placeholder="Re-enter Password" type="password" />
                <Button>
                    {
                        accountType === 'student' ? "Create Student account" : "Create Tutor account"
                    }
                </Button>
            </LoginCard>
        </ThemeBackground>
    }
}

export default SignUpPage;