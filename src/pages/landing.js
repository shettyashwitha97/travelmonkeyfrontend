import React from 'react';

class LandingPage extends React.Component {
    goToPage = (loginTo) => {
        window.location.href = `/login/${loginTo}`;
    }
    render() {
        return <div>
            <h1>Landing Page</h1>
            <button onClick={() => {this.goToPage('student')}}>Log in as student</button>
            <button onClick={() => {this.goToPage('tutor')}}>Log in as tutor</button>
        </div>
    }
}

export default LandingPage;