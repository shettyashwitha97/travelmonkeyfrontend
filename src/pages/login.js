import React from 'react';
import { Heading, CloseButton, DividerWithText, Text, LoginCard, ThemeBackground, Input, LeftAlignLink, Link, Button, ButtonSecondary, Divider } from '../styles/LoginPageStyles';
import CloseIcon from "../assets/close-icon.png"; 

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            accountType: this.props.match.params.accountType
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.accountType !== prevProps.match.params.accountType) {
            this.setState({
                accountType: this.props.match.params.accountType
            })
        }
    }

    singupAcc = () => {
        window.location.href = `/signup/${this.state.accountType}`;
    }

    goToPage = () => {
        window.location.href = "/landing";
    }
    
    render() {
        const { accountType } = this.state;
        return <ThemeBackground>

            <LoginCard>
                <CloseButton src={CloseIcon} onClick={this.goToPage}></CloseButton>
                {/* <BrandLogo src={brandlogo}/> */}
                <Heading>Welcome to TravelMonkey!</Heading>
                <Input placeholder="Username" type="text" />
                <Input placeholder="Password" type="password" />
                <LeftAlignLink href="#">Forgot password?</LeftAlignLink>
                <Button>
                    {
                        accountType === 'student' ? "Login to Student account" : "Login to Tutor account"
                    }
                </Button>
                <DividerWithText>
                    <Text>Do not have an account?</Text>
                    <Divider />
                </DividerWithText>
                
                <ButtonSecondary onClick={this.singupAcc}>
                {
                        accountType === 'student' ? "Sign up as Student" : "Sign up as Tutor"
                }</ButtonSecondary>
            </LoginCard>
        </ThemeBackground>
    }
}

export default LoginPage;